<?php

namespace App\Controller;

use App\Repository\HashtagRepository;
use App\Repository\MessageRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HashtagsController extends AbstractController
{

    private $userRepository;
    private $messageRepository;
    private $entityManager;
    private $hashtagRepository;

    public function __construct(UserRepository $userRepository, MessageRepository $messageRepository, EntityManagerInterface $entityManager, HashtagRepository $hashtagRepository)
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
        $this->messageRepository = $messageRepository;
        $this->hashtagRepository = $hashtagRepository;
    }
    /**
     * @Route("/hashtags/{id}", name="hashtags")
     */
    public function hashtags(int $id)
    {
        $pageHash = $this->hashtagRepository->find($id);
        $messHash = $pageHash->getMessage();

        return $this->render('hashtags/index.html.twig', [
            'hashtag' => $pageHash,
            'messageHash' => $messHash,
        ]);
    }
}
