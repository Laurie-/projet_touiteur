<?php

namespace App\Controller;


use App\Entity\Hashtag;
use App\Entity\Message;
use App\Entity\User;
use App\Form\EditFormType;
use App\Form\MembreType;
use App\Form\MessageType;
use App\Form\RegistrationFormType;
use App\Repository\HashtagRepository;
use App\Repository\MessageRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{

    private $userRepository;
    private $messageRepository;
    private $entityManager;
    private $hashtagRepository;

    public function __construct(UserRepository $userRepository, MessageRepository $messageRepository, EntityManagerInterface $entityManager, HashtagRepository $hashtagRepository)
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
        $this->messageRepository = $messageRepository;
        $this->hashtagRepository = $hashtagRepository;
    }


    /**
     * @Route("/index", name="index")
     */
    public function formIndex(Request $request)
    {
        //Recuprer les messages de la table pour les afficher
        $messageList = $this->messageRepository->findAll();

        //créer un new message avec le form message
        $message = new Message;
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        //Récupérer les hashtags pour les afficher
        $hashtags = $this->hashtagRepository->findAll();


        $h;

        // Ajouter les données dans la BDD
        if ($form->isSubmitted() && $form->isValid()) {
            $message->setDate(new \DateTime());
            $message->setUser($this->getUser());

            //récupérer la list des hashtags
            $hashList = $this->hashtagRepository->findAll();

            //Récupérer la champ hashtag
            $hash = $form->get('hashtags')->getData();


            //Découper la chaine récupérée à partir du #
            $word = explode('#', $hash);
            dump($word);

            //Pour chaque # => créer un new Hashtag, setter le word et add au msg
                foreach ($word as $index => $key){
                    if($index>0){

             //Si le H est deja dans la BDD => juste ajouter au msg
                        $req = $this->hashtagRepository->SearchByWord($key);
                        dump($req);
                        $nbreLignes = count($req);

                        if ($nbreLignes>0){
                            $message->addHashtag($req[0]);

             //sinon, l'ajouter à la BDD et au msg
                        }else {
                            $h = new Hashtag;
                            $h->setWord($key);
                            dump($h);
                            $message->addHashtag($h);

                            $this->entityManager->persist($h);

                        }
                    }
                }
                /*exit;*/

            $this->entityManager->persist($message);
            $this->entityManager->flush();

            return $this->redirectToRoute('index');
        }

        //retourne le twig index (avec liste des msg et l'ajout d'un nouveau msg
        //clé pour form msg et hashtags
        return $this->render('form/index.html.twig', [
            'formIndex' => $form->createView(),
            'messages' => $messageList,
            'hash' => $hashtags,

        ]);


    }

    /**
     * @Route("/profil/{id}", name="profil")
     */
    public function profiles(int $id)
    {

        $page = $this->userRepository->find($id);
        $msg = $page->getMessage();

        $followers = $page->getFollowers();
        $count_followers = count($followers);

        $following = $page->getFollowing();
        $count_following = count($following);


        return $this->render('profiles/index.html.twig', [
            'user' => $page,
            'msg' => $msg,
            'follower' => $followers,
            'count_followers' => $count_followers,
            'count_following' => $count_following,
        ]);
    }


    /**
     * @Route("/follow/{id}", name="follow")
     */

    public function follow(int $id)
    {
        $me = $this->getUser();
        $userFollow = $this->userRepository->find($id);
        $userFollow->addFollower($me);
        $this->entityManager->persist($me);
        $this->entityManager->persist($userFollow);
        $this->entityManager->flush();

        return $this->redirect("/profil/" . $userFollow->getId());
    }


    /**
     * @Route("/edit/{id}", name="edit")
     */

    public function edit(int $id, User $user = null, Request $request): Response
    {
        $editprofil = $this->userRepository->find($id);
        $form = $this->createForm(editFormType::class, $editprofil);
       /* $newprenom = $form->get('prenom')->getData();*/
        $form->handleRequest($request);

        /*$editprofil->setPrenom($newprenom);*/
        $this->entityManager->persist($editprofil);
        $this->entityManager->flush();


        return $this->render('edit/index.html.twig', [
            'user' => $editprofil,
            'editForm' => $form->createView(),
        ]);

    }

// pour supprimer delete à la place de persist
}
