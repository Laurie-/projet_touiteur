<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vich\UploaderBundle\Form\Type\VichImageType;

class EditFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('nom', textType::class,
                [
                    'attr' => ['placeholder' => 'Nouveau nom',
                                'require' => false]
                ])
            ->add('prenom', textType::class,
                [
                    'attr' => ['placeholder' => 'Nouveau prénom',
                                'require' => false]
                ])
            ->add('avatarFile', vichImageType::class , [
                'download_link' => false,
                'allow_delete' => false,
                'image_uri' => false,])

        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
